// sum_app.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include<Windows.h>
#include<iostream>
#include<conio.h>
#include"sum_header.hpp"
using namespace std;
typedef void(__cdecl *Adder)(int a, int b);
void main(){ 
	Adder adder_func;
	HMODULE h_mod;
	h_mod = LoadLibrary(TEXT("C:\\Users\\ansh.arora\\Documents\\Visual Studio 2013\\Projects\\explicit_dynamic_linking\\Debug\\dll_file.dll")); //returns handle to the DLL file
	
	adder_func = (Adder)GetProcAddress(h_mod, "add"); //Getting function address
	int a, b;
	cout << "enter numbers: ";
	cin >> a >> b;
	adder_func(a, b);

	FreeLibrary(h_mod); //Freeing the library
	_getch();

}