#pragma once 
#ifdef DLL_FILE_EXPORTS
#define SUM _declspec(dllexport)
#else
#define SUM _declspec(dllimport)
#endif
SUM void sum(long int a, long int b);